#ga travelling salesman

import numpy as np
import numpy.random as rand
import scipy as sp
import ga_travelling_salesman
'''
Distance_table = np.array([[0,267,602,506,262,282],[0,0,412,467,341,214],[0,0,0,305,467,321],[0,0,0,0,247,243],[0,0,0,0,0,149],[0,0,0,0,0,0]])
Distance_table = Distance_table + Distance_table.T

Distance_table = np.array([[0,1690,2130,4035,2865,1210,3215,755,2750,1430],
                  [1690,0,3060,2770,2415,2755,1525,2435,3770,2930],
                  [2130,3060,0,4320,1840,1295,3495,1735,4390,1030],
                  [4035,2770,4320,0,4125,5100,1965,4780,2415,4885],
                  [2865,2415,1840,4125,0,3140,2795,3235,6015,2870],
                  [1210,2755,1295,5100,3140,0,4230,655,3815,305],
                  [3215,1525,3495,1965,2795,4230,0,3960,4345,4060],
                  [755,2435,1735,4780,3235,655,3960,0,3495,895],
                  [2750,3770,4390,2415,6015,3815,4345,3495,0,3990],
                  [1430,2930,1030,4885,2870,305,4060,895,3990,0]])
'

locations = ['Lund','Kalmar','Stockholm','Karlstad','Goteborg','Jonkoping']

#locations = ['Adel.', 'Alice', 'Brisb', 'Brm', 'Cairns', 'Canb.', 'Darw.', 'Melb.', 'Perth', 'Sydn.']

nbr_of_locations = len(locations) 
'''
mutation_frequency = 0.02

def initiator(size):
    return [[rand.randint(0,nbr_of_locations),rand.randint(0,nbr_of_locations)] for i in range(size)]

def evaluator(gene, phenotype = None):
    if phenotype == None:
        phenotype = make_phenotype(gene)
        phenotype = np.array(phenotype)+1
    pathcost = Distance_table[0,phenotype[0]]
    for index in range(nbr_of_locations-1):
        pathcost += Distance_table[phenotype[index],phenotype[index+1]]
    pathcost += Distance_table[phenotype[-1],0]
    return -pathcost

def mutator(gene):
    for index,elem in enumerate(gene):
        if rand.rand() <= mutation_frequency:
            gene[index] = [rand.randint(0,nbr_of_locations),rand.randint(0,nbr_of_locations)]

    return gene

def make_phenotype(gene):
    phenotype = range(nbr_of_locations)
    for elem in gene:
        i = elem[0]
        j = elem[1]
        phenotype[i],phenotype[j] = phenotype[j],phenotype[i]

    return phenotype

def to_roadmap(org):
    gene = org.gene
    phenotype = make_phenotype(gene)
    phenotype = np.array(phenotype)+1

    roadmap = [locations[0]]

    path = [locations[place] for place in phenotype]

    roadmap.extend(path)

    return roadmap


         
