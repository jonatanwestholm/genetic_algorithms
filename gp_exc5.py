#gp_exc1.py

import numpy as np
from ga_basic import Basic
from ga import Organism
import random
import matplotlib.pyplot as plt

class Linear_GP:

    #========================
    #model default parameters
    #========================

    mutation_frequency = 0.2
    immigration_frequency = 0.01
    mating_share = 0.5
    new_sample_share = 0.1
    crossover_rate = 0.9
    max_generations = 50

    #Custom parameters

    max_arity = 2

    head_length = 7
    tail_length = head_length*(max_arity-1)+1
    gene_length = head_length + tail_length


    x_samples = np.arange(1,3,0.1)
    true_function = lambda x: np.sin(np.pi*x)
    #x_samples = range(10)
    #just pick some random values
    #true_values = 5*np.array([ 0.39616688,  0.72333096,  0.72561549,  0.61790427,  0.62411788,
    #    0.66184887,  0.16643639,  0.62408884,  0.17873299,  0.95730241])
    true_values = np.array([true_function(x) for x in x_samples])
    
    dumb = ['dumb']*3
    variables = ['var0']*5 #gotta get x in there!
    arity0 = [str(x) for x in np.arange(0,10,1)]
    arity1 = ['sin','sqrt','cos','abs','tan','arctan']
    arity2 = ['+','-','*','/']

    tail_array = variables+arity0
    head_array = tail_array+arity1+arity2

    #=================
    #modelling methods
    #=================
    
    #Initiates an entirely new genome 
    #Returns a genome
    def initiator(self):
        head = [random.choice(self.head_array) for x in range(self.head_length)]
        tail = [random.choice(self.tail_array) for x in range(self.tail_length)] 
        return head+tail

    #Computes the fitness of a given genome
    # -the most determining method of the problem model!
    #Returns a number
    def evaluator(self,gene):
        est_values = self.evaluate_expression(gene)
        error = np.sqrt(np.sum((est_values-self.true_values)**2))/self.gene_length
        return 100*np.exp(-error)
        

    def evaluate_expression(self,gene):
        return [self.evaluate_linear_genome(gene,0,[x])[0] for x in self.x_samples] 

    def evaluate_linear_genome(self,gene,pos,var):
        if gene[pos] in self.arity2:
            op = gene[pos]
            ev = self.evaluate_linear_genome(gene,pos+1,var)
            value1 = ev[0]
            new_pos = ev[1]
            ev = self.evaluate_linear_genome(gene,new_pos,var)
            value2 = ev[0]
            new_pos = ev[1]
            if op == '+':
                val = value1+value2
            elif op == '-':
                val = value1-value2
            elif op == '*':
                val = value1*value2
            elif op == '/':
                if value2 == 0:
                    val = 1000000
                else:
                    val = value1/value2
            else:
                print 'error in recursion arity2'
            return (val,new_pos)
        elif gene[pos] in self.arity1:
            op = gene[pos]
            ev = self.evaluate_linear_genome(gene,pos+1,var)
            value = ev[0]
            new_pos = ev[1]
            if op == 'sin':
                val = np.sin(value)
            elif op == 'cos':
                val = np.cos(value)
            elif op == 'tan':
                val = np.tan(value)
            elif op == 'arctan':
                val = np.arctan(value)
            elif op == 'sqrt':
                val = np.sqrt(np.abs(value))
            elif op == 'exp':
                val = np.exp(value)
            elif op == 'abs':
                val = np.abs(value)
            else:
                print 'error in recursion arity1'
            return (val,new_pos)
        else:
            if gene[pos] == 'var0':
                val = var[0]
            elif gene[pos] == 'dumb':
                if pos == self.gene_length-1:
                    return (0,pos) #wtf den skriver ut dumb, where does it do that?
                else:
                    return self.evaluate_linear_genome(gene,pos+1,var)
            else:
                val = float(gene[pos])
            return (val,pos+1)
                
    
    #Mutates a given genome with probability problem.Model.mutation_frequency
    #Returns a genome
    def mutator(self,gene):
        if np.random.random() < self.mutation_frequency:
            place = np.random.randint(self.gene_length)
            if place < self.head_length:
                gene[place] = random.choice(self.head_array)
            else:
                gene[place] = random.choice(self.tail_array)

        return gene
            
    
    #Combines two organisms to a new one
    #Returns a genome
    def recombinator(self,p1,p2):
        if np.random.random() < self.crossover_rate:
            #newgene = Basic.direct_crossover(p1,p2)
            newgene = Basic.double_direct_crossover(p1,p2)
            return newgene
        else:
            return None
  
    #Compares the fitness of two organisms
    #Returns a negative number, 0, or a positive number
    def comparator(self,org1,org2):
        return Basic.comparator(org1,org2)

    def selector(self,population):
        return Basic.random_selection(population,self.mating_share)
        #return Basic.rank_based_selection(population,self.mating_share)
        #return Basic.fp_selection(population,self.mating_share)
    
    #Generates a new population from an old one
    #Returns a new population
    def generation(self,population):
        #mutation
        for org in population:
            org.mutate()

        #sorting
        population.sort(self.comparator)

        #selection
        selected = self.selector(population)
        children = []
        for p1 in selected:
            p2 = random.choice(population)
            while p1 != p2: #organism can only mate with same species
                p2 = random.choice(population)
            child_genome = self.recombinator(p1,p2)
            if child_genome != None:
                children.append(Organism(self,child_genome))

        new_pop = self.reinsertion(population,children)

        #add some new samples
        new_samples = []
        new_sample_number = int(self.new_sample_share*len(population))
        for i in range(new_sample_number):
            new_samples.append(Organism(self))

        return Basic.insert_last(new_pop,new_samples)
   
    def reinsertion(self,population,children):
        #return Basic.insert_last(population,children)
        return Basic.insert_middle(population,children,penalty=0)  

    #Immigrates specimen from pop2 to pop1
    def immigration(self,pop1,pop2):
        return Basic.single_uniform_immigration(pop1,pop2,self.immigration_frequency)  

    def present_statistics(self,world,timer,best_org,best_fit,avg_fit,worst_fit):
        timer = np.array(timer)
        for i in range(len(world)):
            plt.figure()
            best_plot = plt.plot(timer-timer[0],best_fit[i],color='red') #np.array(timer)-t0
            avg_plot = plt.plot(timer-timer[0],avg_fit[i],color='blue')
            worst_plot = plt.plot(timer-timer[0],worst_fit[i],color='green')
            plt.xlabel("Time/s")
            plt.ylabel("Fitness of best (red), average (blue), and worst (green)")
            plt.title("Fitness convergence of island {0:d}".format(i))
            print "Island {0:d} best fit: {1:.4f}".format(i,best_fit[i][-1])

        for i in range(len(world)):
            print "Island {0:d} best genome: {1:s}".format(i,self.gene_to_string(best_org[-1][i]))


        nbr_of_samples = 5
        for i in range(len(world)):
            plt.figure()
            (handle,) = plt.plot(self.x_samples,self.true_values)
            handles = [handle]
            names = ["Target function"]
            #plot every sample_period:th best
            for j in np.linspace(0,self.max_generations-1,nbr_of_samples):
                (handle,) = plt.plot(self.x_samples,self.evaluate_expression(best_org[int(j)][i]))
                handles.append(handle)
                names.append("Best in gen {0:d}".format(int(j)))

            plt.xlabel('x')
            plt.ylabel('Gene expression')
            plt.title('Phenotypic convergence island {0:d}'.format(i))

            plt.legend(handles,names)

        
        plt.show()

    def termination(self,gen_nbr,best_fit,avg_fit):
        return gen_nbr >= self.max_generations
        #return Basic.relatively_unchanged_termination(best_fit,tol=0.99,period=20)
        #return Basic.avg_close_to_best_termination(best_fit,avg_fit,tol = 0.90,period = 20)

    #==================
    #monitoring methods
    #==================
    
    #Often the gene can be interpreted as some string
    #Of course, we want to see our solutions as they evolve
    def gene_to_string(self,gene):
        ev = self.gene_string(gene,0)
        return ev[0]

    def gene_string(self,gene,pos):
        if gene[pos] in self.arity2:
            op = gene[pos]
            ev = self.gene_string(gene,pos+1)
            term1 = ev[0]
            new_pos = ev[1]
            ev = self.gene_string(gene,new_pos)
            term2 = ev[0]
            new_pos = ev[1]
            return ('(('+term1+')'+op+'('+term2+'))',new_pos)
        elif gene[pos] in self.arity1:
            op = gene[pos]
            ev = self.gene_string(gene,pos+1)
            term = ev[0]
            new_pos = ev[1]
            return (op+'('+term+')',new_pos)
        else:
            if gene[pos] == 'var0':
                term = 'x'
            else:
                term = gene[pos]
            return (term,pos+1)

        
    
    #It is good to keep track of how much variation within a population
    def variation_measure(self,population):
        pass



