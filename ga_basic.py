#Some basic GA model and monitoring methods 
#that may be applicable over several problems

import numpy as np 
import random
import scipy as sp 
from collections import Counter

class Basic:

    @staticmethod
    #Fitness Proportionate Selector
    #cfd stands for cumulative distribution function
    def fps(cdf):
        for x in range(1,len(cdf)-1):
            if np.random.random() < (cdf[x]-cdf[x-1])/(1.0-cdf[x-1]):
                return x

        return len(cdf)-1

    @staticmethod
    #Uses basic crossover. Safe for genes of different lengths
    def direct_crossover(org1, org2):
        crossover = random.randint(0,np.amin([len(org1.gene),len(org2.gene)]))
        if np.random.random() <= 0.5:
            newgene = org1.gene[0:crossover]
            newgene.extend(org2.gene[crossover:])
        else:
            newgene = org2.gene[0:crossover]
            newgene.extend(org1.gene[crossover:])

        return newgene

    @staticmethod
    #Uses basic crossover. Safe for genes of different lengths
    def double_direct_crossover(org1, org2):
        crossovers = np.random.randint(0,np.amin([len(org1.gene),len(org2.gene)]),2)
        cross1 = np.min(crossovers)
        cross2 = np.max(crossovers)
        if np.random.random() <= 0.5:
            newgene = org1.gene[0:cross1]
            newgene.extend(org2.gene[cross1:cross2])
            newgene.extend(org1.gene[cross2:])
        else:
            newgene = org2.gene[0:cross1]
            newgene.extend(org1.gene[cross1:cross2])
            newgene.extend(org2.gene[cross2:])

        return newgene

    @staticmethod
    #Betablending for genes of the same size 
    def extrapolation_blending(org1,org2):
        blend = np.random.randint(3)
        if blend == 0:
            newgene = -0.5*org1.gene + 1.5*org2.gene
        elif blend == 1:
            newgene = 0.5*org1.gene + 0.5*org2.gene
        else:
            newgene = 1.5*org1.gene -0.5*org2.gene    
        return newgene

    @staticmethod
    def beta_blending(org1,org2):
        beta = np.random.random()
        return beta*org1.gene + (1-beta)*org2.gene

    @staticmethod
    def random_culling(population, number):
        for i in range(number):
            del population[random.randint(0,len(population))]

        return population
        
    @staticmethod
    #sorts in descending order
    def comparator(org1,org2):
        if org1.fitness <= org2.fitness:
            return 1
        else:
            return -1

    @staticmethod
    #sorts in ascending order
    def neg_comparator(org1,org2):
        if org1.fitness >= org2.fitness:
            return 1
        else:
            return -1

    @staticmethod
    def insert_last(population,children):
        litter_size = len(children)
        population[-litter_size-1:-1] = children
        return population

    @staticmethod
    def insert_middle(population,children,penalty=0):
        litter_size = len(children)
        pop_size = len(population)
        if penalty == 0:
            start = pop_size/2-litter_size/2
        else:
            start = pop_size/2-litter_size/2+pop_size/penalty 
        population[start:start+litter_size] = children
        return population

    @staticmethod
    def random_selection(population,mating_share):
        return [random.choice(population) for i in range(int(np.floor(mating_share*len(population))))]

    @staticmethod
    def rank_based_selection(population,mating_share):
        return population[0:int(np.floor(mating_share*len(population)))]

    @staticmethod
    def fp_selection(population,mating_share):
        cdf = np.array([org.fitness for org in population],dtype=float)
        cdf = cdf/np.sum(cdf)
        cdf = np.cumsum(cdf)
        selected = []
        for i in range(int(np.floor(mating_share*len(population)))):
            selected.append(population[Basic.fps(cdf)])

        return selected
    '''
    def elitist_generation(population,comparator=None, selector=None, nbr_of_offspring = 5):
        if comparator == None:
            comparator = ga_trivial.comparator
        if selector == None:
            selector = ga_trivial.selector
        for org in population:
            org.mutate()
            org.evaluate()

        population.sort(comparator)
        cdf = np.array([org.fitness for org in population],dtype=float)
        cdf = cdf/np.sum(cdf)
        cdf = np.cumsum(cdf)

        children = []
        for i in range(nbr_of_offspring):
            parent1 = population[selector(cdf)]
            parent2 = population[selector(cdf)]

            offspring = parent1.mate_with(parent2)
            offspring.evaluate()
            children.append(offspring)

        new_pop = population[:-nbr_of_offspring]
        new_pop.extend(children)
        return new_pop
    
    @staticmethod
    def random_death_generation(population,comparator=None, selector=None, nbr_of_offspring = 5):
        if comparator == None:
            comparator = ga_trivial.comparator
        if selector == None:
            selector = ga_trivial.selector
        for org in population:
            if org.mutate():
                org.evaluate()

        population.sort(comparator)
        cdf = [org.fitness for org in population]
        #print np.diff(np.array(cdf))
        cdf.insert(0,0)
        cdf = np.array(cdf,dtype=float)
        cdf = cdf/np.sum(cdf)
        cdf = np.cumsum(cdf)
        children = []
        n = len(population)
        for i in range(nbr_of_offspring):
            parent1 = population[ga_trivial.selector(cdf)-1]
            parent2 = population[ga_trivial.selector(cdf)-1]

            offspring = parent1.mate_with(parent2)
            children.append(offspring)

        population.extend(children)
        return random_culling(population, nbr_of_offspring)
    '''
    @staticmethod
    #Potentially immigrates a single random specimen from pop2 to pop1
    def single_uniform_immigration(pop1,pop2,immigration_frequency):
        if np.random.random() < immigration_frequency:
            x = np.random.randint(len(pop1))
            y = np.random.randint(len(pop2))
            pop1[x] = pop2[y]

        return pop1

    @staticmethod
    def relatively_unchanged_termination(best_fit,tol = 0.98, period = 10):
        if len(list(best_fit)) > period:
            bf = np.array(best_fit)
            return np.all(bf[-period:]/bf[0] > tol)
        else:
            return False

    @staticmethod
    def avg_close_to_best_termination(best_fit,avg_fit,tol = 0.9,period = 10):
        if len(list(best_fit)) > period:
            bf = np.array(best_fit)
            af = np.array(avg_fit)
            return np.all(af[-period:]/bf[-period:] > tol)
        else:
            return False
            

'''
if __name__ == '__main__':
    number = 100
    a = random.rand(number)
    a[0] = 0
    a = a/np.sum(a)
    print a
    cdf = np.cumsum(a)    
    size = 10000
    b = np.zeros(size)
    for i in range(size):
        b[i] = selector(cdf)

    counter = [float(sum(b == i)) for i in range(number)]
    print counter
'''















