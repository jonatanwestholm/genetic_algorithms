#ga voting salesman

import numpy as np
import numpy.random as np_random
import random
import scipy.stats as stats 
import heapq
import ga_travelling_salesman

'''
Distance_table = np.array([[0,267,602,506,262,282],[0,0,412,467,341,214],[0,0,0,305,467,321],[0,0,0,0,247,243],[0,0,0,0,0,149],[0,0,0,0,0,0]])
Distance_table = Distance_table + Distance_table.T

Distance_table = np.array([[0,1690,2130,4035,2865,1210,3215,755,2750,1430],
                  [1690,0,3060,2770,2415,2755,1525,2435,3770,2930],
                  [2130,3060,0,4320,1840,1295,3495,1735,4390,1030],
                  [4035,2770,4320,0,4125,5100,1965,4780,2415,4885],
                  [2865,2415,1840,4125,0,3140,2795,3235,6015,2870],
                  [1210,2755,1295,5100,3140,0,4230,655,3815,305],
                  [3215,1525,3495,1965,2795,4230,0,3960,4345,4060],
                  [755,2435,1735,4780,3235,655,3960,0,3495,895],
                  [2750,3770,4390,2415,6015,3815,4345,3495,0,3990],
                  [1430,2930,1030,4885,2870,305,4060,895,3990,0]])

locations = ['Lund','Kalmar','Stockholm','Karlstad','Goteborg','Jonkoping']

#locations = ['Adel.', 'Alice', 'Brisb', 'Brm', 'Cairns', 'Canb.', 'Darw.', 'Melb.', 'Perth', 'Sydn.']
'''

mutation_frequency = 0.02
prio_size = 4

def initiator(size):
    gene = {}
    init_size = 3
    for i in range(size):
        gene[i] = np_random.randint(1,size, prio_size)

    return gene

def evaluator(gene):
    phenotype = make_phenotype(gene)
    phenotype = np.array(phenotype)
    return ga_travelling_salesman.evaluator([],phenotype)

def make_phenotype(gene):
    sequence = []
    remaining = {}
    for i in range(1,len(gene)):
        remaining[i] = i
    place = 0
    while len(sequence) < len(gene):
        priority = gene[place]
        i = 0
        next = None
        while next == None and i < prio_size:
            try:
                next = remaining[priority[i]]
            except KeyError:
                i += 1

        if next == None:
            try:
                next = random.choice(remaining.keys())
            except IndexError:
                next = 0

        sequence.append(next)
        #print len(remaining)
        #print next
        #print remaining
        if next != 0:
            del remaining[next]

        place = next
    
    return sequence
            

def recombinator(org1,org2):
    new_gene = {}
    g1 = org1.gene
    g2 = org2.gene
    for i in range(len(g1)):
        p1 = g1[i]
        p2 = g2[i]
        crossover = np_random.randint(0,np.amin([len(p1),len(p2)]))
        if np_random.rand() <= 0.5:
            newbase = list(p1[0:crossover])
            newbase.extend(p2[crossover:])
        else:
            newbase = list(p2[0:crossover])
            newbase.extend(p1[crossover:])
        
        new_gene[i] = newbase

    return new_gene

def mutator(gene):
    mut = np_random.rand() <= mutation_frequency
    if mut:
        gene_length = len(gene)
        place = np_random.randint(0,gene_length)
        base = gene[place]
        base_length = len(base)
        x = np_random.rand()
        if x <= 0.2:
            base[np_random.randint(0,base_length)] = np_random.randint(1,gene_length)
        else:
            i = np_random.randint(0,base_length)
            j = np_random.randint(0,base_length)
            base[i],base[j] = base[j],base[i]

        gene[place] = base

    return (mut,gene)

def to_roadmap(org):
    gene = org.gene
    phenotype = make_phenotype(gene)
    phenotype = np.array(phenotype)

    roadmap = [locations[0]]

    path = [locations[place] for place in phenotype]

    roadmap.extend(path) 

    return roadmap

















    
