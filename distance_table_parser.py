#distance table parser 

import numpy as np

def parse_file(filename):
    f = open(filename, 'rb')
    f = f.read().split('\n')
    locations = f[0].split()
    table = np.array([ np.array(f[i].split()).astype('double') for i in range(1,len(f)-1)])
    return (table, locations)

if __name__ == '__main__':
    (table, locations) = parse_file('MontenegroDistance.txt')
    print locations,"\n", table
    i = 0
    for elem in table:
        print len(list(elem))
        i += 1
    print "i: ",i
