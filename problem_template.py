#A template for GA problems 
#Can be used with ga.py

import numpy as np
#from ga_basic import Basic

#========================
#model default parameters
#========================

mutation_frequency = 0.00
immigration_frequency = 0.00
generation_coefficient = 0.00

class Template:
    
    #=================
    #modelling methods
    #=================
    
    #Initiates an entirely new genome 
    #Returns a genome
    def initiator(self):
        return 'aardvark'

    #Computes the fitness of a given genome
    # -the most determining method of the problem model!
    #Returns a number
    def evaluator(self,gene):
        pass
    
    #Mutates a given genome with probability problem.Model.mutation_frequency
    #Returns a genome
    def mutator(self,gene):
        pass
    
    #Combines two organisms to a new one
    #Returns a genome
    def recombinator(self,p1,p2):
        pass
    
    #Compares the fitness of two organisms
    #Returns a negative number, 0, or a positive number
    def comparator(self,org1,org2):
        pass
    
    #Generates a new population from an old one
    #Returns a new population
    def generation(self,population):
        pass

    #Immigrates specimen from pop2 to pop1
    def immigration(self,pop1,pop2)
        pass  

    def reinsertion(self,population,children):  
        pass

    
    #==================
    #monitoring methods
    #==================
    
    #Often the gene can be interpreted as some string
    #Of course, we want to see our solutions as they evolve
    def gene_to_string(self,gene):
        pass
    
    #It is good to keep track of how much variation within a population
    def variation_measure(self,population):
        pass

class Modified_Template(Template):
    
    @staticmethod
    def mutator(self,gene):
        #different mutator that you want to try out
        pass    
    






