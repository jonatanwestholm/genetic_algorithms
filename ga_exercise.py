#A problem module for the GA exercises
#2015-09-11

import numpy as np
import random
from ga_basic import Basic
from ga import Organism
import matplotlib.pyplot as plt

class Exc1:

    #========================
    #model default parameters
    #========================

    mutation_frequency = 0.04
    generation_coefficient = 0.05
    crossover_rate = 0.9
    mating_share = 0.5
    elite_share = 0.1 #share that will be spared from mutation

    max_generations = 0
    
    #=================
    #modelling methods
    #=================
    
    def initiator(self):
        return 6*np.random.random(2)-3

    def evaluator(self,gene):
        return 1.0/(1+gene[0]**2+gene[1]**2)
    
    def mutator(self,gene):
        if np.random.random() < self.mutation_frequency:
            gene += 1.0*np.random.random(2)-0.5
            gene = (gene+3)%6-3

        return gene
    
    def recombinator(self,org1,org2):
        if np.random.random() < self.crossover_rate:
            newgene = Basic.beta_blending(org1,org2) 
            return (newgene+3)%6-3
        else:
            return None
    
    def comparator(self,org1,org2):
        return Basic.comparator(org1,org2)
    
    #Selects which organisms from a given population should get to mate 
    def selector(self,population):
        return population[0:int(np.floor(self.mating_share*len(population)))]
        
        #random selection
        #return [random.choice(population) for i in range(int(np.floor(self.mating_share*len(population))))]

    def generation(self,population):
        #mutation
        pop_mut = population[int(np.floor(self.elite_share*len(population))):-1]
        for org in pop_mut:
            org.mutate()

        #sorting
        population.sort(self.comparator)

        #selection
        selected = self.selector(population)
        children = []
        for p1 in selected:
            p2 = random.choice(population)
            while p1 is p2: #organism cannot mate with itself
                p2 = random.choice(population)
            child_genome = self.recombinator(p1,p2)
            if child_genome != None:
                children.append(Organism(self,child_genome))

        return self.reinsertion(population,children)

    def reinsertion(self,population,children):
        return Basic.insert_last(population,children)

    def termination(self,gen_nbr,best_fit,avg_fit): 
        return gen_nbr >= self.max_generations 

    #==================
    #monitoring methods
    #==================
    
    def gene_to_string(self,gene):
        return str(gene)
    
    def variation_measure(self,population):
        return 0
    
    def present_statistics(self,world,timer,best_org,best_fit,avg_fit,worst_fit):
        timer = np.array(timer)
        for i in range(len(world)):
            plt.figure()
            best_plot = plt.plot(timer-timer[0],best_fit[i],color='red') #np.array(timer)-t0
            avg_plot = plt.plot(timer-timer[0],avg_fit[i],color='blue')
            worst_plot = plt.plot(timer-timer[0],worst_fit[i],color='green')
            plt.xlabel("Time/s")
            plt.ylabel("Fitness of best (red), average (blue), and worst (green)")
            print best_fit[i][-1]

            self.plot_population(world[i].population,"Island {0:d}, Final population".format(i))
        
        for final_solution in best_org[-1]:
            print self.gene_to_string(final_solution)
        
        plt.show()

    def plot_population(self,pop,title):
        plt.figure()
        x_coords = [org.gene[0] for org in pop]
        y_coords = [org.gene[1] for org in pop]
        plt.scatter(x_coords,y_coords)
        plt.title(title)

class Exc2(Exc1):
    
    elite_share = 0.1 #share that will be spared from mutation

    def selector(self,population):
        cdf = np.array([org.fitness for org in population],dtype=float)
        cdf = cdf/np.sum(cdf)
        cdf = np.cumsum(cdf)
        selected = []
        for i in range(int(np.floor(self.mating_share*len(population)))):
            selected.append(population[Basic.fps(cdf)])

        return selected
        
    def recombinator(self,org1,org2):
        if np.random.random() < self.crossover_rate:
            newgene = Basic.extrapolation_blending(org1,org2) 
            return (newgene+3)%6-3
        else:
            return None 

    def termination(self,gen_nbr,best_fit,avg_fit):
        return Basic.relatively_unchanged_termination(best_fit,tol=0.99,period=20)
        #return Basic.avg_close_to_best_termination(best_fit,avg_fit,tol = 0.90,period = 20)

class Exc3(Exc1):

    immigration_frequency = 0.00
    elite_share = 0.0 #share that will be spared from mutation
    nbr_of_species = 5
    
    def initiator(self):
        gene = 10*np.random.random(3)
        gene[2] = np.random.randint(self.nbr_of_species)
        return gene
    
    def evaluator(self,gene):
        x = gene[0]
        y = gene[1]
        return -(y*np.sin(4*x)+10*x*np.sin(2*y))

    def recombinator(self,org1,org2):
        species = org1.gene[2]
        if np.random.random() < self.crossover_rate:
            newgene = Basic.extrapolation_blending(org1,org2) 
            newgene = newgene%10
            newgene[2] = species
            return newgene
        else:
            return None 

    def mutator(self,gene):
        if np.random.random() < self.mutation_frequency:
            gene[:2] += 1.0*np.random.random(2)-0.5
            gene[:2] %= 10

        return gene

    def generation(self,population):
        #mutation
        pop_mut = population[int(np.floor(self.elite_share*len(population))):-1]
        for org in pop_mut:
            org.mutate()

        #sorting
        population.sort(self.comparator)

        #selection
        selected = self.selector(population)
        children = []
        for p1 in selected:
            p2 = random.choice(population)
            while p1.gene[2] != p2.gene[2]: #organism can only mate with same species
                p2 = random.choice(population)
            child_genome = self.recombinator(p1,p2)
            if child_genome != None:
                children.append(Organism(self,child_genome))

        return self.reinsertion(population,children)

    def reinsertion(self,population,children):
        return Basic.insert_last(population,children)
        #return Basic.insert_middle(population,children)

    def immigration(self,pop1,pop2):
        return Basic.single_uniform_immigration(pop1,pop2,self.immigration_frequency)   

    #def termination(self,gen_nbr,best_fit,avg_fit):
    #    return Basic.relatively_unchanged_termination(best_fit,tol=0.99,period=100)
    #    #return Basic.avg_close_to_best_termination(best_fit,avg_fit,tol = 0.90,period = 20) 


    def present_statistics(self,world,timer,best_org,best_fit,avg_fit,worst_fit,best_of_species):
        timer = np.array(timer)
        for i in range(len(world)):
            plt.figure()
            best_plot = plt.plot(timer-timer[0],best_fit[i],color='red') #np.array(timer)-t0
            avg_plot = plt.plot(timer-timer[0],avg_fit[i],color='blue')
            worst_plot = plt.plot(timer-timer[0],worst_fit[i],color='green')
            plt.xlabel("Time/s")
            plt.ylabel("Fitness of best (red), average (blue), and worst (green)")
            print best_fit[i][-1]

            self.plot_population(world[i].population,"Island {0:d}, Final population".format(i))

        plt.figure()
        for i in range(self.nbr_of_species):
            plt.plot(timer-timer[0],best_of_species[i])
        
        plt.xlabel('Time/s')
        plt.ylabel('Fitness of best per species')
        
        
        for final_solution in best_org[-1]:
            print self.gene_to_string(final_solution)
        
        plt.show()

    def plot_population(self,pop,title):
        plt.figure()
        x_coords = [org.gene[0] for org in pop]
        y_coords = [org.gene[1] for org in pop]
        plt.scatter(x_coords,y_coords)
        plt.title(title)

    def get_best_of_species(self,population):
        best_of_species = []
        for i in range(self.nbr_of_species):
            best = 0 #for this problem, it's likely that it will be better than 0
            for org in population:
                if org.gene[2] == i and org.fitness > best:
                    best = org.fitness

            best_of_species.append(best)

        return best_of_species






