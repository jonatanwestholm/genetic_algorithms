#ga_langsound.py

import numpy as np 
import numpy.random as nprandom
import random

dictionary = open("pron_dict.txt","r").read()
counts = Counter(dictionary)

mutation_frequency = 0.01

alphabet = range(33,154) #should contain all characters in phonetic alphabet
alphabet.remove(127) #dont include delete character

class Langsound:

    def evaluator(word):
        #unigram version
        fitness = 0
        for character in word:
            fitness += counts[chr(character)]
        
        return fitness

    def recombinator(org1, org2):
        #don't use this one right now     
        pass

    def mutator(gene):
        mut = False
        if nprandom.random() < mutation_frequency:
            mut = True
            place = nprandom.randint(len(gene))
            gene[place] = random.choice(alphabet)

        return (mut,gene)

    def initiator(size):
        #do use size input
        #size = nprandom.randint(4,9)
        word = [random.choice(alphabet) for i in range(size)]
        return word

    def gene_to_string(gene):
        return ''.join(map(chr,gene))

    def variation_measure(gene_list):
        var = 0
        transposed = map(list,map(None,*gene_list))
        for elem in transposed:
            counts = Counter(elem)
            counts[None] = 0
            var += sum(np.sqrt(counts.values())) #sum the square roots of the counts

        return var
            





















