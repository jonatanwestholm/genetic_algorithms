#gp_exc1.py

import numpy as np
from ga_basic import Basic
from ga import Organism
import random
import matplotlib.pyplot as plt

class Polynomial_w_operators:

    #========================
    #model default parameters
    #========================

    mutation_frequency = 0.1
    immigration_frequency = 0.02
    mating_share = 0.5
    new_sample_share = 0.1
    crossover_rate = 0.9
    max_generations = 50

    #Custom parameters

    min_gene_base = -10
    max_gene_base = 10
    gene_length = 7

    x_samples = np.arange(0.1,2,0.2)
    true_function = lambda x: 1/x#np.sin(x*np.pi)
    true_values = np.array([true_function(x) for x in x_samples])
    
    #=================
    #modelling methods
    #=================
    
    #Initiates an entirely new genome 
    #Returns a genome
    def initiator(self):
        return np.random.uniform(self.min_gene_base,self.max_gene_base,self.gene_length) 

    #Computes the fitness of a given genome
    # -the most determining method of the problem model!
    #Returns a number
    def evaluator(self,gene):
        est_values = self.evaluate_polynomial(gene)
        error = np.sqrt(np.sum((est_values-self.true_values)**2))/self.gene_length
        return 100*np.exp(-error)

    def evaluate_polynomial(self,gene):
        evens = range(0,self.gene_length,2)
        odds = range(1,self.gene_length,2)
        coefficients = gene[evens]
        operators = gene[odds]
        op_strings = [self.op_nbr_to_str(op) for op in operators]

        est_values = np.zeros(len(self.x_samples))
        i = 0
        for x,true_y in zip(self.x_samples,self.true_values):
            #the constant
            value_str = str(coefficients[0])
            for j in range(len(coefficients)-1):
                value_str += "{0:s}({1:.4f}*({2:.4f}**{3:d}))".format(op_strings[j],coefficients[j+1],x,j+1)
            #print value_str
            #here comes the magic line!!
            exec "est_value = {0:s}".format(value_str)
            est_values[i] = est_value
            i += 1

        return est_values

    def op_nbr_to_str(self,op):
        if op < -5:
            return '*'
        elif op > -5 and op < 5:
            return '+'
        else:
            return '-'
    
    #Mutates a given genome with probability problem.Model.mutation_frequency
    #Returns a genome
    def mutator(self,gene):
        if np.random.random() < self.mutation_frequency:
            place = np.random.randint(self.gene_length)
            #gene[place] = float(np.random.uniform(self.min_gene_base,self.max_gene_base,1))
            gene[place] += np.random.random()*(self.max_gene_base-self.min_gene_base)/10
            gene[place] = (gene[place] - self.min_gene_base)%(self.max_gene_base-self.min_gene_base) + self.min_gene_base

        return gene
            
    
    #Combines two organisms to a new one
    #Returns a genome
    def recombinator(self,p1,p2):
        if np.random.random() < self.crossover_rate:
            newgene = Basic.extrapolation_blending(p1,p2)
            newgene = (newgene - self.min_gene_base)%(self.max_gene_base-self.min_gene_base) + self.min_gene_base

            return newgene
        else:
            return None
  
    #Compares the fitness of two organisms
    #Returns a negative number, 0, or a positive number
    def comparator(self,org1,org2):
        return Basic.comparator(org1,org2)

    def selector(self,population):
        return Basic.random_selection(population,self.mating_share)
        #return Basic.rank_based_selection(population,self.mating_share)
        #return Basic.fp_selection(population,self.mating_share)
    
    #Generates a new population from an old one
    #Returns a new population
    def generation(self,population):
        #mutation
        for org in population:
            org.mutate()

        #sorting
        population.sort(self.comparator)

        #selection
        selected = self.selector(population)
        children = []
        for p1 in selected:
            p2 = random.choice(population)
            while p1 != p2: #organism can only mate with same species
                p2 = random.choice(population)
            child_genome = self.recombinator(p1,p2)
            if child_genome != None:
                children.append(Organism(self,child_genome))

        return self.reinsertion(population,children)
   
    def reinsertion(self,population,children):
        #return Basic.insert_last(population,children)
        return Basic.insert_middle(population,children,penalty=5)  

    #Immigrates specimen from pop2 to pop1
    def immigration(self,pop1,pop2):
        return Basic.single_uniform_immigration(pop1,pop2,self.immigration_frequency)  

    def present_statistics(self,world,timer,best_org,best_fit,avg_fit,worst_fit):
        timer = np.array(timer)
        for i in range(len(world)):
            plt.figure()
            best_plot = plt.plot(timer-timer[0],best_fit[i],color='red') #np.array(timer)-t0
            avg_plot = plt.plot(timer-timer[0],avg_fit[i],color='blue')
            worst_plot = plt.plot(timer-timer[0],worst_fit[i],color='green')
            plt.xlabel("Time/s")
            plt.ylabel("Fitness of best (red), average (blue), and worst (green)")
            plt.title("Fitness convergence of island {0:d}".format(i))
            print "Island {0:d} best fit: {1:.4f}".format(i,best_fit[i][-1])

        for i in range(len(world)):
            print "Island {0:d} best genome: {1:s}".format(i,self.gene_to_string(best_org[-1][i]))


        nbr_of_samples = 5
        for i in range(len(world)):
            plt.figure()
            (handle,) = plt.plot(self.x_samples,self.true_values)
            handles = [handle]
            names = ["Target function"]
            #plot every sample_period:th best
            for j in np.linspace(0,self.max_generations-1,nbr_of_samples):
                (handle,) = plt.plot(self.x_samples,self.evaluate_polynomial(best_org[int(j)][i]))
                handles.append(handle)
                names.append("Best in gen {0:d}".format(int(j)))

            plt.xlabel('x')
            plt.ylabel('Gene expression')
            plt.title('Phenotypic convergence island {0:d}'.format(i))

            plt.legend(handles,names)

        
        plt.show()

    def termination(self,gen_nbr,best_fit,avg_fit):
        return gen_nbr >= self.max_generations
        #return Basic.relatively_unchanged_termination(best_fit,tol=0.99,period=20)
        #return Basic.avg_close_to_best_termination(best_fit,avg_fit,tol = 0.90,period = 20)

    #==================
    #monitoring methods
    #==================
    
    #Often the gene can be interpreted as some string
    #Of course, we want to see our solutions as they evolve
    def gene_to_string(self,gene):
        evens = range(0,self.gene_length,2)
        odds = range(1,self.gene_length,2)
        coefficients = gene[evens]
        operators = gene[odds]
        op_strings = [self.op_nbr_to_str(op) for op in operators]

        value_str = str(coefficients[0])
        for i in range(len(coefficients)-1):
            value_str += "{0:s}({1:.4f}*(x**{2:d}))".format(op_strings[i],coefficients[i+1],i+1)

        return value_str

        
    
    #It is good to keep track of how much variation within a population
    def variation_measure(self,population):
        pass



