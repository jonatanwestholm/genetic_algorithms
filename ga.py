#Main GA module. 
#The goal is to make this module as versatile as possible
#so that to solve a problem, you need only 
#modify the dynamically imported module

import numpy as np
import matplotlib.pyplot as plt 
import matplotlib.patches as mpatches
import time
import argparse

import traceback

class Organism:

    def __init__(self, model, gene = None):
        if gene == None:
            gene = model.initiator()
        self.gene = gene
        self.evaluator = model.evaluator
        self.mutator = model.mutator
        self.recombinator = model.recombinator
        self.evaluate()

    def evaluate(self):
        self.fitness = self.evaluator(self.gene)

    def mutate(self):
        self.gene = self.mutator(self.gene)
        self.evaluate()

    def mate_with(self,other):
        newgene = self.recombinator(self, other)
        return Organism(model,newgene)

class Island:

    def __init__(self,population,model):
        self.population = population
        self.model = model

    def generation(self):
        self.population = self.model.generation(self.population)

    def immigrate(self,other_island):
        self.population = self.model.immigration(self.population,other_island.population)

    def average_fitness(self):
        fit = 0
        for org in self.population:
            fit += org.fitness

        return fit*1.0/len(self.population)

def main(args):

    problem = __import__(args.problem)
    Model = getattr(problem,args.model)
    model = Model()
    if args.immigration_frequency != None:
        model.immigration_frequency = float(args.immigration_frequency)

    if args.mutation_frequency != None:
        model.mutation_frequency = float(args.mutation_frequency)

    if args.crossover_rate != None:
        model.crossover_rate = float(args.crossover_rate)

    model.max_generations = int(args.nbr_of_generations)
    
    nbr_of_islands = int(args.nbr_of_islands)
    init_pop_size = int(args.init_pop_size)
    nbr_of_generations = int(args.nbr_of_generations)

    init_world = [Island([ Organism(model) for i in range(init_pop_size)], model) for j in range(nbr_of_islands)]

    world = init_world
    best_fit = []
    avg_fit = []
    worst_fit = []
    best_org = []
    best_of_species = []
    t0 = time.clock()
    timer = []
    variation = []
    gen_nbr = 0

    '''i = 0
    for island in world:
        model.plot_population(island.population,"Island {0:d}, Initial population".format(i))
        i += 1
    '''
    try:
        while not model.termination(gen_nbr,best_fit,avg_fit):
            if gen_nbr%100 == 0:
                print "\nAll orgs in island {0:d} generation {1:d}\n".format(0,gen_nbr)
                for org in world[0].population:
                    print model.gene_to_string(org.gene)
    
            gen_best = []
            gen_avg = []
            gen_worst = []
            gen_best_org =[]
            for isle in world:
                if gen_nbr%10 == 0:
                    pass
                    #model.present_population(isle.population)
                isle.generation()
                #gene_list.extend([org.gene for org in isle.population])
                gen_best.append(isle.population[0].fitness)
                gen_worst.append(isle.population[-1].fitness)
                gen_avg.append(isle.average_fitness())
                gen_best_org.append(isle.population[0].gene)
                if nbr_of_islands == 1:
                    try:
                        #only take last island for now
                        best_of_species.append(model.get_best_of_species(isle.population))
                    except AttributeError:
                        pass #better to ask forgiveness than permission
                if nbr_of_islands > 1:
                    for other_isle in world:
                        isle.immigrate(other_isle)
                #print model.gene_to_string(isle.population[0].gene).decode('latin-1')
            best_fit.append(gen_best)
            avg_fit.append(gen_avg)
            worst_fit.append(gen_worst)
            best_org.append(gen_best_org)

            gen_nbr += 1
            #print gene_list[0]
            timer.append(time.clock())
        '''
        for isle in world:
            print "\n"
            for i in range(20):
                #population = select(population,comparator = comp, nbr_of_offspring = init_pop_size/10)
                #progress.append(population[0].fitness)
                #timer.append(time.clock())
                #gene_list = [org.gene for org in population]
                #variation.append(model.variation_measure(gene_list))
                print isle.population[i].gene#model.gene_to_string(isle.population[i].gene).decode('latin-1')
        '''
    except KeyboardInterrupt:
        traceback.print_exc()

    '''
    print "Before: "
    for org in init_pop:
        print org.gene
    
    print "After: "
    for org in population:
        print org.gene, org.fitness
    '''
    #print progress
    #trivial.print_monitor()
    #transposed_progress = map(list,map(None,*progress))
    best_fit = map(list,map(None,*best_fit))
    avg_fit = map(list,map(None,*avg_fit))
    worst_fit = map(list,map(None,*worst_fit))
    if best_of_species != []:
        best_of_species = map(list,map(None,*best_of_species))
        model.present_statistics(world,timer,best_org,best_fit,avg_fit,worst_fit,best_of_species)
    else:
        model.present_statistics(world,timer,best_org,best_fit,avg_fit,worst_fit)
    #'''
    '''
    plt.figure(nbr_of_islands)    
    plt.plot(range(nbr_of_generations),np.array(variation))
    plt.xlabel("Generation")
    plt.ylabel("Variation")
    '''


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--problem',dest="problem",help="name of the python file where the models are")
    parser.add_argument('--model',dest="model",help="name of the evolution model to use")
    parser.add_argument('--islands',dest="nbr_of_islands",default=1)
    parser.add_argument('--popsize',dest="init_pop_size",help="initial populations size per island",default=50)
    parser.add_argument('--generations',dest="nbr_of_generations",default=50)
    parser.add_argument('--immigration',dest="immigration_frequency",default=None)
    parser.add_argument('--mutation',dest="mutation_frequency",default=None)
    parser.add_argument('--crossover',dest="crossover_rate",default=None)
    args = parser.parse_args()
    main(args)
