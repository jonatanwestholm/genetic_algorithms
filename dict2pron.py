# -*- coding: latin1 -*-
import re
import time
import sys
def xml2entries(f):
    return re.findall('<lemma-entry>.{0,410}</lemma-entry>',f,re.DOTALL)
    
def get_pronounciation(f):
    words = []
    i = 0
    for entry in f:
        i = i+1
        pron = re.search('<pronunciation>.{0,40}</pronunciation>',entry,re.DOTALL)
        try:
            pron = pron.group(0)
            pron = pron[16:-17]
            words.append(pron)
        except Exception:
            print i

    return words
        

def print2pron(f):
    g = open("pron_dict.txt","w")
    for entry in f:
        g.write("{0:s}\n".format(entry))

def dict2arff(filename):
    t = time.clock()
    f = open(filename).read().decode('latin1') #f is now a string
    f = xml2entries(f) #f is now a list of strings
    f = get_pronounciation(f)
    print2pron(f)
    print time.clock() -t


if __name__ == '__main__':
    reload(sys)
    sys.setdefaultencoding('latin-1')
    dict2arff('dictionary.txt')
