#genetic algorithm 1

import numpy as np 
import scipy as sp 
import matplotlib.pyplot as plt 

mutation_frequency = 0.02

def binary_fitness(x):
	return sum([ x[-(i+1)]*(2**i) for i in range(len(x))])

def fitness_proportionate_selector(cdf):
	i = -1
	for x in cdf:
		i = i + 1
		if np.random.rand() <= x:
			return i

	return i

def generate_population(population,comparator, selector, nbr_of_offspring = 5):
	for org in population:
		org.mutate()
		org.evaluate()

	population.sort(comparator)
	cdf = np.array([org.fitness for org in population],dtype=float)
	cdf = cdf/np.sum(cdf)
	cdf = np.cumsum(cdf)

	children = []
	for i in range(nbr_of_offspring):
		parent1 = population[selector(cdf)]
		parent2 = population[selector(cdf)]

		offspring = parent1.mate_with(parent2)
		offspring.evaluate()
		children.append(offspring)

	new_pop = population[:-nbr_of_offspring]
	new_pop.extend(children)
	return new_pop
	

class Organism:

	def __init__(self, evaluator, gene = None,size = 0):
		if gene == None:
			gene = list(np.random.randint(0,2,size))
		self.gene = gene
		self.size = len(self.gene)
		self.eval = evaluator

	def evaluate(self):
		self.fitness = self.eval(self.gene)

	def mutate(self):
		for i in range(len(self.gene)):
			if(np.random.rand() <= mutation_frequency):
				self.gene[i] = 1 - self.gene[i]

	def mate_with(self,other):
		crossover = np.random.randint(0,self.size)
		if np.random.rand() <= 0.5:
			newgene = self.gene[0:crossover]
			newgene.extend(other.gene[crossover:])
		else:
			newgene = other.gene[0:crossover]
			newgene.extend(self.gene[crossover:])

		return Organism(self.eval,gene = newgene)


def main():
	compare = lambda org1,org2 : int(- org1.fitness + org2.fitness)
	init_pop_size = 50
	nbr_of_generations = 200

	init_pop = [ Organism(binary_fitness,size = 10) for i in range(init_pop_size)]
	init_pop2 = []
	population = init_pop
	progress = []
	for i in range(nbr_of_generations):
		population = generate_population(population,compare,fitness_proportionate_selector,20)
		progress.append(population[0].fitness)

	
	print "Before: "
	for org in init_pop:
		print org.gene
	
	print "After: "
	for org in population:
		print org.gene, org.fitness
	
	plt.plot(progress)
	plt.show()

if __name__ == '__main__':
	main()
